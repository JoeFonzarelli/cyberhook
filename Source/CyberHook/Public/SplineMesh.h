// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SplineMeshComponent.h"
#include "Components/SplineComponent.h"
#include "Components/SceneComponent.h"
#include "SplineMesh.generated.h"

UCLASS()
class CYBERHOOK_API ASplineMesh : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplineMesh();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category= Spline)
	USplineComponent* Spline;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = Spline)
	USceneComponent* SceneRoot;

	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	TArray< USplineMeshComponent* > SplineMeshes;
	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
	void DestroyMeshes();
};
