// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "CyberHookGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class CYBERHOOK_API UCyberHookGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
		float HookMaxLength = 1000;
};
