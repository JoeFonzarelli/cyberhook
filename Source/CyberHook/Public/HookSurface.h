// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/StaticMeshComponent.h"
#include "HookSurface.generated.h"

UCLASS()
class CYBERHOOK_API AHookSurface : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHookSurface();
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Mesh)
	UStaticMeshComponent* Mesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Default)
	AActor* projectile;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default)
	float StartSpeedOverride = 0; //if 0, don't apply speed override to the pawn and use the one defined in the projectile instead

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintCallable, Category = "Custom")
	void OnHookAttached(class AActor* shotProjectile);

};
