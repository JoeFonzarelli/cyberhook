// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "Components/StaticMeshComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/BoxComponent.h"
#include "PhysicsEngine/PhysicsConstraintComponent.h"
#include "GameFramework/PawnMovementComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "Kismet/GameplayStatics.h"
#include "CustomPlayer.generated.h"

UCLASS()
class CYBERHOOK_API ACustomPlayer : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	ACustomPlayer();

	//VARIABLES
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float SpeedModifierGround = 1; //speed while walking or jumping

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float SpeedModifierHook = 0.5; //speed while hooked

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float maxSpeedGround;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float maxSpeedAir;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float maxZSpeed; //used for regulating the jump force while on air

	//use this and the variable below to define the value of the force applied while jumping. the higher the vertical velocity, the lesser the force ( v = 0 -> f = max, v = maxSpeed -> f = min)
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float minJumpForce;
	
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float maxJumpForce;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = InputParameters)
	float RotationSpeed;

	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class ACyberHookProjectile> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Hook)
	class ACyberHookProjectile* spawnedHook;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = Hook)
	bool isHookPressed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hook)
	bool isHooked;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hook)
	float hookRetrieveTime;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hook)
	float minHookLength; //don't allow the hook to be shorter than this 

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float StandardPawnMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float HookedPawnMass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float StandardFOV;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float HookedFOV;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float FOVSpeed;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float minVignetting;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Feeling)
	float maxVignetting;

	float hookRetrieveAccumulatedTime;
	FVector GroundNormalVector;
	int numberOfOverlaps = 0; //how many overlaps is the player performing
	bool isOverlappingSphere = false; //if it's hanging from a rope in the air, the player is going to overlap the hook sphere
	bool allowJump = false; //allow jumping once even if we are in the air, but only one time at every ground overlap. Allow one jump when the player leaves the rope too.
	FVector2D input; //value of the axis used for the movement of the player
	bool isJumping = false;
	float airJumpModifier = 2; //how much will be the jump force be multiplied by when jumping while in the air
	float targetFOV; //What FOV we want to achieve, depending on the player speed and it's hooked state



	UPROPERTY(BlueprintReadWrite)
	float StandardLinearDamping; //linear damping to use when on ground (we change it to 0.1 when on the hook sphere to simulate frictionless pendulus movement

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	bool isOverlappingFloor;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hook)
		FVector front;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Hook)
		FVector right;
protected:


	//CUSTOM FUNCTIONS
	void MoveForward(float value);
	void MoveSides(float value);
	void YawRotation(float value); //z axis (look to the sides)
	void PitchRotation(float value); //y axis (look to top/bot)

	void Jump();
	
	void Fire();

	void RetrieveHookPressed();
	void RetrieveHookReleased();

	//COMPONENTS
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite) //use visibleanywhere for components instead of editanywhere
	UStaticMeshComponent* staticMesh;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UCapsuleComponent* Capsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite)
	UBoxComponent* Box;
	
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FirstPersonCameraComponent;

	//UPROPERTY(BlueprintReadWrite, Category = Mesh)
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Mesh)
	class UStaticMeshComponent* HookOrigin;
		
public:	

	//STANDARD EVENTS
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION()
	void BeginOverlap(UPrimitiveComponent* OverlappedComponent,	AActor* OtherActor,	UPrimitiveComponent* OtherComp,	int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult);

	UFUNCTION()
	void EndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	UFUNCTION(BlueprintImplementableEvent, Category = "Custom")
	void BPTick(bool retrievingHook); //fake a tick event in blueprints so we can use both the c++ tick and the blueprints tick. parameter indicates if we removed the hook in this frame

	UFUNCTION(BlueprintImplementableEvent, Category = "Custom")
	void SetPhysMat(bool onGround); //switch the physical material depending on the state of the player

};
