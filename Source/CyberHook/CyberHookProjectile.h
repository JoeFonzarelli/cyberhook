// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "SplineMesh.h"
#include "Public/CustomPlayer.h"
#include "Public/HookSurface.h"
#include "CyberHookProjectile.generated.h"

UCLASS(config=Game)
class ACyberHookProjectile : public AActor
{
	GENERATED_BODY()

	/** collision component */
	UPROPERTY(VisibleAnywhere, Category = Projectile)
	class UBoxComponent* CollisionComp;

	/** Projectile movement component */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Movement, meta = (AllowPrivateAccess = "true"))
	class UProjectileMovementComponent* ProjectileMovement;

public:

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Default)
	float SwingStartForce = -1000;

	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = Data)
	class ASplineMesh* splineMesh;


	ACyberHookProjectile();

	/** called when projectile hits something */
	UFUNCTION()
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

	UFUNCTION(BlueprintImplementableEvent, Category = "Custom")
	void StraightenSpline();

	/** Returns CollisionComp subobject **/
	FORCEINLINE class UBoxComponent* GetCollisionComp() const { return CollisionComp; }
	/** Returns ProjectileMovement subobject **/
	FORCEINLINE class UProjectileMovementComponent* GetProjectileMovement() const { return ProjectileMovement; }

protected:
	virtual void Destroyed();
};

