#include "CyberHookProjectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Components/BoxComponent.h"
#include "Engine.h"
#include "Editor.h"
#include "Engine/EngineTypes.h"
#include "string"
#include "Components/SceneComponent.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"

#define print(X) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT( X ))

ACyberHookProjectile::ACyberHookProjectile() 
{
	// Use a sphere as a simple collision representation
	CollisionComp = CreateDefaultSubobject<UBoxComponent>(TEXT("CollisionComp"));
	CollisionComp->BodyInstance.SetCollisionProfileName("Projectile");
	CollisionComp->OnComponentHit.AddDynamic(this, &ACyberHookProjectile::OnHit);		// set up a notification for when this component hits something blocking

	// Players can't walk on it
	CollisionComp->SetWalkableSlopeOverride(FWalkableSlopeOverride(WalkableSlope_Unwalkable, 0.f));
	CollisionComp->CanCharacterStepUpOn = ECB_No;

	// Set as root component
	RootComponent = CollisionComp;

	// Use a ProjectileMovementComponent to govern this projectile's movement
	ProjectileMovement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileComp"));
	ProjectileMovement->UpdatedComponent = CollisionComp;
	ProjectileMovement->InitialSpeed = 3000.f;
	ProjectileMovement->MaxSpeed = 3000.f;
	ProjectileMovement->bRotationFollowsVelocity = true;
	ProjectileMovement->bShouldBounce = true;

	// Die after 3 seconds by default
	InitialLifeSpan = 3.0f;
}

void ACyberHookProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	//stick the object to the collided surface
	if ((OtherActor != NULL) && ((OtherActor != this) || OtherActor == GetWorld()->GetFirstPlayerController()))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, OtherActor->GetName());
		//attach hook only on surfaces with the hookSurface tag. destory in any other case
		if (OtherActor->ActorHasTag(FName(TEXT("hookSurface")))) {

			AHookSurface* actor_scr = ((AHookSurface*)OtherActor);
			ProjectileMovement->Deactivate();

			actor_scr->OnHookAttached(this);

			//override the start speed of the player
			if (actor_scr->StartSpeedOverride != 0) SwingStartForce = actor_scr->StartSpeedOverride;

			CollisionComp->OnComponentHit.RemoveDynamic(this, &ACyberHookProjectile::OnHit); //stop calling this function
			StraightenSpline(); //delete the parabolic spline and create a straight one + change material color
		}
		else {
			Destroy();
		}

	}
}

void ACyberHookProjectile::Destroyed() {

	Super::Destroyed();

	//1. destroy the splinemesh attached
	if (splineMesh != NULL) splineMesh->Destroy();

	////2. set the reference of the hook on the player to null
	if (GetWorld() != NULL) {
		APlayerController* controller = GetWorld()->GetFirstPlayerController();
		if (controller != NULL) {
			APawn* character = controller->GetPawn();

			if (character != NULL) {
				ACustomPlayer * cyberhookCharacter = (ACustomPlayer*)character;

				if (cyberhookCharacter != NULL)
					cyberhookCharacter->spawnedHook = NULL;
			}
		}
	}
}