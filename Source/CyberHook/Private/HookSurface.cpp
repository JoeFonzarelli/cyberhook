// Fill out your copyright notice in the Description page of Project Settings.


#include "HookSurface.h"

// Sets default values
AHookSurface::AHookSurface()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("BaseMesh"));
	Mesh->AttachTo(GetRootComponent());

	Tags.Add(FName(TEXT("hookSurface")));
}

// Called when the game starts or when spawned
void AHookSurface::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AHookSurface::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


void AHookSurface::OnHookAttached(AActor* shotProjectile) {
	this->projectile = shotProjectile;
	shotProjectile->AttachToActor(this, FAttachmentTransformRules::KeepWorldTransform);
}

