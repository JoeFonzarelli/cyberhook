﻿#include "CustomPlayer.h"
#include "../CyberHookProjectile.h"
#include "Camera/CameraComponent.h"
#include "GameFramework/Controller.h"
#include "Components/InputComponent.h"

#define print(X) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT( X ))

float Remap(float value, float originMin, float originMax, float destinationMin, float destinationMax, bool clamp = false) { //(https://stackoverflow.com/questions/3451553/value-remapping)
	float retVal = destinationMin + (value - originMin) * (destinationMax - destinationMin) / (originMax - originMin);
	
	if (clamp) {
		if (retVal > destinationMax) retVal = destinationMax;
		else if (retVal < destinationMin) retVal = destinationMin;
	}

	return retVal;
}

// Sets default values
ACustomPlayer::ACustomPlayer()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	AutoPossessPlayer = EAutoReceiveInput::Player0;
	
	Capsule = CreateDefaultSubobject<UCapsuleComponent> (TEXT("Capsule"));
	Capsule->SetSimulatePhysics(true);
	Capsule->SetEnableGravity(true);
	Capsule->SetCollisionProfileName(TEXT("BlockAll"));
	Capsule->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	RootComponent = Capsule;

	Box = CreateDefaultSubobject<UBoxComponent>(TEXT("BoxTrigger"));
	Box->SetSimulatePhysics(false);
	Box->SetEnableGravity(false);
	Box->SetCollisionEnabled(ECollisionEnabled::QueryAndPhysics);
	Box->SetCollisionProfileName(TEXT("OverlapAll"));
	Box->SetupAttachment(Capsule);
	Box->SetGenerateOverlapEvents(true);
	
	FirstPersonCameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("FirstPersonCamera"));
	FirstPersonCameraComponent->SetRelativeLocation(FVector(-39.56f, 1.75f, 64.f)); // Position the camera
	FirstPersonCameraComponent->bUsePawnControlRotation = true;
	FirstPersonCameraComponent->SetupAttachment(Capsule);

	staticMesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("StaticMesh"));
	staticMesh->SetupAttachment(FirstPersonCameraComponent);

	HookOrigin = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("Hook Origin"));
	HookOrigin->SetupAttachment(staticMesh);
}

// Called when the game starts or when spawned
void ACustomPlayer::BeginPlay()
{
	Super::BeginPlay();

	Capsule->BodyInstance.bLockXRotation = true;
	Capsule->BodyInstance.bLockYRotation = true;
	Capsule->BodyInstance.bLockZRotation = true;

	Capsule->BodyInstance.bLockRotation = true;

	Capsule->OnComponentHit.AddDynamic(this, &ACustomPlayer::OnHit);

	Box->OnComponentBeginOverlap.AddDynamic(this, &ACustomPlayer::BeginOverlap);
	Box->OnComponentEndOverlap.AddDynamic(this, &ACustomPlayer::EndOverlap);

	StandardLinearDamping = Capsule->GetLinearDamping();

	targetFOV = StandardFOV;
}

void ACustomPlayer::MoveForward(float value)
{
	if (value != 0) {
		FVector forwardVector;
		if (isOverlappingFloor || isHooked) { //if the player is in ground, project the rotation of the player on the horizon axis so we get the lookat direction without taking into account the angle of the camera
			FVector rotationVector = (Controller->GetControlRotation() + FRotator(0, 90, 0)).Vector(); //get the right vector so we can get the front with the cross product below
			rotationVector.Z = 0;
			forwardVector = FVector::CrossProduct(rotationVector, GroundNormalVector); //get the front vector, making it dependent on the angle of the ground surface we are in
		}
		else { //we are in the air (not hooked)
			forwardVector = Controller->GetControlRotation().Vector(); //just move along the front vector, parallelly to the horizon
			forwardVector.Z = 0;
		}

		front = forwardVector;

		forwardVector.Normalize();
		FVector finalForce = value * 1000 * Capsule->GetMass() * forwardVector;

		if (isHooked) {
			finalForce *= SpeedModifierHook;
			if (finalForce.Z > 0)
				finalForce.Z = 0; //avoid applying force upwards while hooked (avoid the "climbing" effect along the hook sphere surface) so we accomplish a more realistic result of what would happen when hanging from a rope in the air
		}
		else finalForce *= SpeedModifierGround;

		Capsule->AddForce(finalForce);
	}

	input.Y = value;
}

void ACustomPlayer::MoveSides(float value)
{
	if (value != 0) {

		FVector rightVector;
		if (isOverlappingFloor || isHooked) { //if the player is in ground, project the rotation of the player on the horizon axis so we get the lookat direction without taking into account the angle of the camera
			FVector rotationVector = Controller->GetControlRotation().Vector(); //get the front vector so we can calculate the right vector with the cross product below
			rotationVector.Z = 0;
			rightVector = FVector::CrossProduct(GroundNormalVector, rotationVector); //make the right vector dependent on the angle of the surface we are in
		}
		else { //we are in the air (not hooked)
			rightVector = (Controller->GetControlRotation() + FRotator(0, 90, 0)).Vector(); //just move along the right vector
			rightVector.Z = 0;
		}

		right = rightVector;

		rightVector.Normalize();
		FVector finalForce = value * 1000 * Capsule->GetMass() * rightVector;

		if (isHooked) {
			finalForce *= SpeedModifierHook;
			if (finalForce.Z > 0)
				finalForce.Z = 0; //avoid applying force upwards while hooked (avoid the "climbing" effect along the hook sphere surface) so we accomplish a more realistic result of what would happen when hanging from a rope in the air
		}
		else finalForce *= SpeedModifierGround;

		Capsule->AddForce(finalForce);
	}

	input.X = value;
}

void ACustomPlayer::YawRotation(float value)
{
	if (value != 0) {
		Controller->SetControlRotation(Controller->GetControlRotation() + FRotator(0, value*RotationSpeed, 0));
	}
}

void ACustomPlayer::PitchRotation(float value)
{
	if (value != 0) {
		Controller->SetControlRotation((Controller->GetControlRotation() + FRotator(-value*RotationSpeed, 0, 0)));
	}
}

void ACustomPlayer::Jump() {
	if (allowJump && !isHooked) { //allow jumping once even if in the air

		//calculate the force of the jump
		float JumpForce = Remap(Capsule->GetComponentVelocity().Z, 0, maxZSpeed, maxJumpForce, minJumpForce, true);
		
		//apply the jump impulse
		Capsule->AddForce(FVector(0, 0, 1000 * Capsule->GetMass()*JumpForce*(isOverlappingFloor? 1 : airJumpModifier) + Capsule->GetComponentVelocity().Z));
		allowJump = false; 
		isJumping = true;
	}
}

void ACustomPlayer::Fire()
{
	// try and fire a projectile
	if (ProjectileClass != NULL)
	{
		if (spawnedHook != NULL || isHookPressed) return; //allow only one projectile at a time

		UWorld* const World = GetWorld();
		if (World != NULL)
		{
			const FRotator SpawnRotation = Controller->GetControlRotation();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = HookOrigin->GetComponentLocation();// +SpawnRotation.RotateVector(HookOrigin->GetComponentLocation());

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

			// spawn the projectile at the muzzle
			spawnedHook = World->SpawnActor<ACyberHookProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);
		}
	}
}


void ACustomPlayer::RetrieveHookPressed() {
	this->isHookPressed = true;
	this->hookRetrieveAccumulatedTime = GetWorld()->GetTimeSeconds();
}

void ACustomPlayer::RetrieveHookReleased() {
	this->isHookPressed = false;
}

// Called every frame
void ACustomPlayer::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	//ADJUST VELOCITY
	FVector vel = Capsule->GetComponentVelocity();
	vel.Z = 0; //don't take into account the vertical speed (jump/fall)

	float maxSpeed;
	if (isOverlappingSphere) maxSpeed = maxSpeedAir;
	else maxSpeed = maxSpeedGround;
			
	if (vel.Size() > maxSpeed) { //cap the speed of the player, if required

		vel.Normalize();
		vel *= maxSpeed;

		vel.Z = Capsule->GetComponentVelocity().Z;

		Capsule->SetPhysicsLinearVelocity(vel);
	}

	bool HookRetrieved = false;
	//RETRIEVE HOOK
	//if the hook has been launched, delete it if the RetrieveHook button is pressed for at least HookretrieveTime seconds
	if (isHookPressed && spawnedHook != NULL) {
		if (GetWorld()->GetTimeSeconds() - this->hookRetrieveAccumulatedTime >= this->hookRetrieveTime) {
			this->hookRetrieveAccumulatedTime = 0;
			spawnedHook->Destroy();
			spawnedHook = NULL;
			isHooked = false;
			HookRetrieved = true;
			allowJump = true;
			Capsule->SetMassOverrideInKg( FName("NAME_None"), StandardPawnMass);
			targetFOV = StandardFOV;
			FirstPersonCameraComponent->PostProcessSettings.VignetteIntensity = minVignetting;
			FirstPersonCameraComponent->PostProcessSettings.bOverride_VignetteIntensity = true;
		}
	}

	//MOVEMENT ON GROUND
	if (isOverlappingFloor && input.X == 0 && input.Y == 0) Capsule->SetPhysicsLinearVelocity(FVector(0, 0, isJumping?Capsule->GetComponentVelocity().Z:0)); //if there is no input, and the player is grounded, stop it


	BPTick(HookRetrieved);
	if (isHooked) {
		//Define the target fov depending on the current speed of the pawn
		//perform a map range from speed (0 to maxAirSpeed) to FOV (standard to air) (https://stackoverflow.com/questions/3451553/value-remapping)
		targetFOV = Remap(Capsule->GetComponentVelocity().Size(), 0, maxSpeedAir, StandardFOV, HookedFOV); //StandardFOV + (Capsule->GetComponentVelocity().Size()) * (HookedFOV - StandardFOV) / (maxSpeedAir);

		//set the vignetting intensity depending on the speed 
		FirstPersonCameraComponent->PostProcessSettings.VignetteIntensity = Remap(Capsule->GetComponentVelocity().Size(), 0, maxSpeedAir, minVignetting, maxVignetting); //minVignetting + (Capsule->GetComponentVelocity().Size()) * (maxVignetting - minVignetting) / (maxSpeedAir);
		FirstPersonCameraComponent->PostProcessSettings.bOverride_VignetteIntensity = true;

		//while the player is grounded, use the standard mass. 
		if (isOverlappingFloor)
			Capsule->SetMassOverrideInKg(FName("NAME_None"), StandardPawnMass);
		else
			Capsule->SetMassOverrideInKg(FName("NAME_None"), HookedPawnMass);
	}

	//lerp towards the target FOV
	if (std::abs(FirstPersonCameraComponent->FieldOfView - targetFOV) > 0.05f) {
		//lerp towards the targetFOV
		float currentFOV = FirstPersonCameraComponent->FieldOfView;
		float newFOV = currentFOV + FOVSpeed * (targetFOV - currentFOV);
		
		FirstPersonCameraComponent->FieldOfView = newFOV;
	}
}

void ACustomPlayer::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	GroundNormalVector = NormalImpulse;
	NormalImpulse.Normalize();
}


void ACustomPlayer::BeginOverlap(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult &SweepResult) 
{
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {
		isOverlappingSphere = true;
		SetPhysMat(false); //set sphere physical material
	}
	else {
		++numberOfOverlaps;
		if (numberOfOverlaps == 1) { //first overlap. we started overlapping a first object 
			isOverlappingFloor = true;
			allowJump = true;
			SetPhysMat(true); //set ground physical material
		}
	}
	isJumping = false;
}

void ACustomPlayer::EndOverlap(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex) 
{
	if (OtherComp->GetCollisionObjectType() == ECollisionChannel::ECC_GameTraceChannel3) {
		isOverlappingSphere = false;
		SetPhysMat(false);
	}
	else {
		--numberOfOverlaps;
		if (numberOfOverlaps == 0) {
			isOverlappingFloor = false;
			SetPhysMat(false);
		}
	}
}

// Called to bind functionality to input
void ACustomPlayer::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Movement actions
	PlayerInputComponent->BindAxis("MoveForward", this, &ACustomPlayer::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ACustomPlayer::MoveSides);
	PlayerInputComponent->BindAxis("Turn", this, &ACustomPlayer::YawRotation);
	PlayerInputComponent->BindAxis("TurnRate", this, &ACustomPlayer::YawRotation);
	PlayerInputComponent->BindAxis("LookUp", this, &ACustomPlayer::PitchRotation);
	PlayerInputComponent->BindAxis("LookUpRate", this, &ACustomPlayer::PitchRotation);

	PlayerInputComponent->BindAction("Jump", IE_Pressed, this, &ACustomPlayer::Jump);

	//Hook actions
	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ACustomPlayer::Fire);
	PlayerInputComponent->BindAction("RetractHook", IE_Pressed, this, &ACustomPlayer::RetrieveHookPressed);
	PlayerInputComponent->BindAction("RetractHook", IE_Released, this, &ACustomPlayer::RetrieveHookReleased);
}

