// Fill out your copyright notice in the Description page of Project Settings.


#include "SplineMesh.h"

#define print(X) GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Blue, TEXT( X ))

// Sets default values
ASplineMesh::ASplineMesh()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	SceneRoot = CreateDefaultSubobject<USceneComponent>(TEXT("DefaultSceneRoot"));

	Spline = CreateDefaultSubobject<USplineComponent>(TEXT("Spline"));
	Spline->SetupAttachment(SceneRoot);
}

// Called when the game starts or when spawned
void ASplineMesh::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ASplineMesh::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

